from models import Term, DatedTerm
from pydantic import ValidationError
import pytest


def test_term_model_perfect_scenarion():
    t = Term(term="cancer")
    assert t.term == "cancer"


def test_term_model_missing_term():
    with pytest.raises(ValidationError):
        Term()


@pytest.mark.parametrize(
    ("term", "start_year", "end_year", "raises"),
    (
        ("cancer", 1999, 2022, None),  # perfect
        ("cancer", 1999, 1999, None),  # boundry
        ("cancer", 1999, 1998, ValidationError),
        ("cancer", 199, 1998, ValidationError),
        ("cancer", 1999, 198, ValidationError),
    ),
)
def test_dated_term_model(
    term: str, start_year: int, end_year: int, raises: Exception | None
):
    if raises:
        with pytest.raises(raises):
            DatedTerm(term=term, start_year=start_year, end_year=end_year)
        return
    DatedTerm(term=term, start_year=start_year, end_year=end_year)
