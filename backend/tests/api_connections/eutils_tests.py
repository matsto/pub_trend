from api_connections import eutils as e

from unittest.mock import Mock


def test_get_trend_data_perfect_scenario():
    e.get_data = Mock(return_value={"esearchresult": {"count": 10}})

    resp = e.get_trend_data("cancer", 1999)
    assert resp == {1999: 10}


def test_get_trend_data_malformed_response():
    e.get_data = Mock(return_value={"esearchresult": {"error": "KnownBug"}})

    resp = e.get_trend_data("cancer", 1999)
    assert resp == {1999: 0}


def test_get_trend_data_wrong_type_response():
    e.get_data = Mock(return_value={"esearchresult": {"count": "a10"}})

    resp = e.get_trend_data("cancer", 1999)
    assert resp == {1999: 0}


def test_get_last_year_pub_count_perfect_scenario():
    e.get_data = Mock(return_value={"esearchresult": {"count": 10}})

    resp = e.get_last_year_pub_count("cancer")
    assert resp == 10
