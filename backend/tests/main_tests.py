from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

# high level E2E tests
# they require Internet connection


def test_read_main():
    response = client.get("/")
    assert response.status_code == 404


def test_ping():
    response = client.get("/ping")
    assert response.status_code == 200
    assert response.json() == "pong"


def test_get_trend():
    response = client.get("/publications/trend")
    assert response.status_code == 405


def test_get_trend_post_meth():
    response = client.post(
        "/publications/trend",
        json={"term": "covid", "start_year": 2015, "end_year": 2022},
    )

    assert response.status_code == 200
    assert response.json() == {
        "2017": 1,
        "2018": 1,
        "2016": 0,
        "2021": 137072,
        "2015": 0,
        "2019": 50,
        "2022": 98596,
        "2020": 90918,
    }
