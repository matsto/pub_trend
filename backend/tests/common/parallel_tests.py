import pytest

from common import parallel as p


def test_perfect_scenario():
    tasks = [
        (sum, [[1, 2]]),
        (sum, [[1, 2, 3]]),
        (sum, [[1, 2, 3, 4]]),
        (sum, [[1, 2, 3, 4, 5]]),
    ]

    resp = p.run_tasks_concurrently(tasks)
    assert sorted(resp) == [3, 6, 10, 15]


def test_wrong_arg():
    tasks = [
        (print, 2),
    ]
    with pytest.raises(TypeError):
        p.run_tasks_concurrently(tasks)
