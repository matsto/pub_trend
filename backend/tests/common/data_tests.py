from unittest.mock import Mock

import pytest

from common import data as d


@pytest.mark.parametrize(
    ("url", "format", "timeout", "raises"),
    (
        (
            # perfect scenario with json resp
            "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&datetype=pdat&retmode=json&term=cancer+AND+2016&rettype=count",
            d.Format.json,
            1,
            None,
        ),
        (
            # perfect scenario with xml resp
            "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=36169019&retmode=xml",
            d.Format.xml,
            1,
            None,
        ),
    ),
)
def test_data(url: str, format: d.Format, timeout: int, raises: Exception | None):
    resp = d.get_data(url, format, timeout)
    assert isinstance(resp, dict)


def test_timeout():
    class Resp:
        status_code = 2014

    d.requests.get = Mock(return_value=Resp)
    with pytest.raises(TimeoutError):
        d.get_data("not_important", d.Format.json, 1)
