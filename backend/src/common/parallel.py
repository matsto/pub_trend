import concurrent.futures as concurrent
from typing import Any, Callable, Iterable, List, Tuple


def run_tasks_concurrently(
    tasks: Iterable[Tuple[Callable[..., Any], Iterable[Any]]], workers=4
) -> List[Any]:
    with concurrent.ThreadPoolExecutor(max_workers=workers) as executor:
        futures: List[concurrent.Future[Any]] = [
            executor.submit(func, *args) for func, args in tasks
        ]

    return [f.result() for f in concurrent.as_completed(futures)]
