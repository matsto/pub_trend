from enum import Enum, auto
from time import time
from typing import Any, Dict

import requests
import xmltodict


class Format(Enum):
    json = auto()
    xml = auto()


def get_data(url: str, format: Format = Format.json, timeout=60) -> Dict[str, Any]:
    st = time()
    while True:
        if timeout and time() > st + timeout:
            raise TimeoutError
        resp = requests.get(url)
        if resp.status_code != 200:
            continue
        return resp.json() if format == Format.json else xmltodict.parse(resp.content)
