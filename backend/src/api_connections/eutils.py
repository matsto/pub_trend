from typing import Dict, List

from common.data import Format, get_data

BASE_URL = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils"
ESEARCH_BASE_URL = f"{BASE_URL}/esearch.fcgi"
EFETCH_BASE_URL = f"{BASE_URL}/efetch.fcgi"

COMMON_PARAMS = "db=pubmed&datetype=pdat&retmode=json"


def _get_pub_count(url: str) -> int:
    data = get_data(url)
    try:
        publications_no = int(data["esearchresult"]["count"])
    except (ValueError, KeyError):
        publications_no = 0
    return publications_no


def get_trend_data(term: str, year: int) -> Dict[int, int]:
    url = f"{ESEARCH_BASE_URL}?{COMMON_PARAMS}&term={term}&mindate={year}&maxdate={year}&rettype=count"
    return {year: _get_pub_count(url)}


def get_last_year_pub_count(term: str) -> int:
    url = f"{ESEARCH_BASE_URL}?{COMMON_PARAMS}&term={term}&reldate=365&rettype=count"
    return _get_pub_count(url)


def get_last_year_pub_ids(term: str, ret_start=0, ret_max=1e5):
    url = (
        f"{ESEARCH_BASE_URL}?{COMMON_PARAMS}&term={term}"
        f"&reldate=365&retstart={ret_start}&retmax={int(ret_max)}"
    )
    data = get_data(url)

    try:
        publications = data["esearchresult"]["idlist"]
    except KeyError:
        publications = []
    return publications


def get_pub_institutions(pub_ids: List[str]):
    ids = ",".join(pub_ids)
    url = f"{EFETCH_BASE_URL}?db=pubmed&id={ids}&retmode=xml"
    data = get_data(url, format=Format.xml)
    publications = data["PubmedArticleSet"]["PubmedArticle"]
    return data
