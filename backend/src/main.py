from collections import ChainMap
from typing import Any, Dict

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api_connections.eutils import get_trend_data
from common.constants import Endpoints
from common.parallel import run_tasks_concurrently
from models import DatedTerm, Term

app = FastAPI()

origins = [
    "http://matesto.site",
    "https://matesto.site",
    "http://localhost",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/ping")
def root():
    return "pong"


@app.post(f"{Endpoints.pubs}{Endpoints.trend}")
def get_trend(item: DatedTerm) -> Dict[int, int]:
    _range = range(item.start_year, item.end_year + 1)
    tasks = [(get_trend_data, (item.term, y)) for y in _range]
    workers = max(len(_range) // 4, 1)
    result = run_tasks_concurrently(tasks, workers=workers)

    return ChainMap(*result)


@app.post(f"{Endpoints.pubs}{Endpoints.trend}{Endpoints.instit}/last_year")
def get_last_year_instit(item: Term) -> Dict[Any, Any]:
    # TODO
    pass
