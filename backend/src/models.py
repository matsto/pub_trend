from pydantic import BaseModel, validator, root_validator
from datetime import date


class Term(BaseModel):
    term: str


class DatedTerm(Term):
    start_year: int
    end_year: int

    @root_validator
    def years_validator(cls, values):
        sy, ey = values.get("start_year"), values.get("end_year")

        if sy is not None and ey is not None and sy > ey:
            raise ValueError(
                "start year has to be less or equal than the end year."
            )
        return values

    @validator("start_year", "end_year")
    def check_year(cls, v):
        curr_year = date.today().year
        if v > curr_year:
            raise ValueError(
                "PubMed doesn't have a crystal ball. "
                f"Value should be less or equal than {curr_year}."
            )

        if v < 1000:
            raise ValueError("PubMed accepts 4-digit year notation only.")
        return v
