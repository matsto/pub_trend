class Term {
  late String term;
  late int startYear;
  late int endYear;

  Term();

  Map<String, dynamic> toJson() {
    return {
      "term": term,
      "start_year": startYear,
      "end_year": endYear,
    };
  }
}
