import 'package:fluent_ui/fluent_ui.dart';
import 'package:frontend/contents/home_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const NavigationView(
      content: HomeScreenContent(),
      appBar: NavigationAppBar(
          title: Text('Publication Trend'), leading: Icon(FluentIcons.chart)),
    );
  }
}
