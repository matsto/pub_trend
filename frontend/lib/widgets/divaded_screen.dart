import 'package:fluent_ui/fluent_ui.dart';

class DividedScreen extends StatelessWidget {
  const DividedScreen(
      {super.key, required this.left, required this.right, this.ratio = 1 / 3});
  final Widget left;
  final Widget right;
  final double ratio;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(18.0, 0, 18, 0),
      child: Row(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * ratio,
            child: left,
          ),
          const SizedBox(
            width: 18,
          ),
          Expanded(child: right)
        ],
      ),
    );
  }
}
