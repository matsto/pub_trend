import 'package:d_chart/d_chart.dart';
import 'package:dio/dio.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:frontend/models/term.dart';
import 'package:frontend/widgets/divaded_screen.dart';

class HomeScreenContent extends StatefulWidget {
  const HomeScreenContent({super.key});

  @override
  State<HomeScreenContent> createState() => _HomeScreenContentState();
}

class _HomeScreenContentState extends State<HomeScreenContent> {
  final _formKey = GlobalKey<FormState>();
  final term = Term();
  final dio = Dio();
  Map<String, int>? chartData;

  String? validateYear(String? text) {
    if (text == null || text.isEmpty) {
      return 'Provide a valid term';
    }
    try {
      int.parse(text);
    } catch (e) {
      return 'The year has to be a numeric value';
    }
    return null;
  }

  void submit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      var payload = term.toJson();
      Response _response;

      try {
        _response = await dio.post(
            'https://matesto.site:8000/publications/trend',
            data: payload);
      } catch (e) {
        e as DioError;
        final String? msg = e.response?.data['detail'][0]['msg'];

        showDialog(
          context: context,
          builder: (context) {
            return ContentDialog(
              title: const Text('Error'),
              content: (msg != null)
                  ? Text(msg)
                  : const Text("Unknown error occured"),
              actions: [
                Button(
                  child: const Text('Ok'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            );
          },
        );
        return;
      }
      final data = _response.data.cast<String, int>();

      setState(() {
        chartData = data;
      });
    }
  }

  Widget drawChart() {
    if (chartData == null) {
      return const Center(
        child: Text("Nothing to display yet"),
      );
    } else {
      final List<Map<String, dynamic>> sortedChartData = [];
      if (chartData != null) {
        final sortedKeys = chartData!.keys.toList();
        sortedKeys.sort((a, b) => int.parse(a).compareTo(int.parse(b)));
        for (var k in sortedKeys) {
          final int? val = chartData![k];
          sortedChartData.add({'domain': k, 'measure': val});
        }
      }
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 18.0, 0, 18.0),
        child: Column(
          children: [
            Text(term.term),
            Expanded(
              child: DChartBar(
                data: [
                  {
                    'id': 'Bar',
                    'data': sortedChartData,
                  },
                ],
                domainLabelPaddingToAxisLine: 16,
                axisLineTick: 2,
                axisLinePointTick: 2,
                axisLinePointWidth: 10,
                axisLineColor: Colors.green,
                measureLabelPaddingToAxisLine: 16,
                barColor: (barData, index, id) => Colors.green,
                showBarValue: true,
              ),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return DividedScreen(
      left: Padding(
        padding: const EdgeInsets.only(top: 80.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormBox(
                placeholder: 'Term',
                validator: (text) {
                  if (text == null || text.isEmpty) {
                    return 'Provide a valid term';
                  }
                },
                onSaved: (String? newValue) => (newValue != null)
                    ? setState(() {
                        term.term = newValue;
                      })
                    : {},
              ),
              const SizedBox(height: 18),
              const Text("Published between"),
              TextFormBox(
                placeholder: 'From',
                validator: (text) => validateYear(text),
                onSaved: (String? newValue) => (newValue != null)
                    ? setState(() {
                        term.startYear = int.parse(newValue);
                      })
                    : {},
              ),
              TextFormBox(
                placeholder: 'To (included)',
                validator: (text) => validateYear(text),
                onSaved: (String? newValue) => (newValue != null)
                    ? setState(() {
                        term.endYear = int.parse(newValue);
                      })
                    : {},
              ),
              const SizedBox(height: 18),
              FilledButton(
                onPressed: submit,
                child: const Text('Submit'),
              ),
            ],
          ),
        ),
      ),
      right: drawChart(),
      ratio: 1 / 3,
    );
  }
}
