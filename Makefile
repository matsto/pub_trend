# init
init:
	python3 -m venv venv
	./venv/bin/pip install -U pip wheel
	./venv/bin/pip install docker-compose
	./venv/bin/pip install -r requirements-dev.txt

# docker

up:
	./venv/bin/docker-compose up -d

down:
	./venv/bin/docker-compose down --remove-orphans

prune:
	docker system prune -a
	docker volume prune

build:
	./venv/bin/docker-compose build --parallel

logs:
	./venv/bin/docker-compose logs -f

restart_rebuild: down build up

restart: down up

# local

run_server_locally:
	PYTHONPATH=${PWD}/backend/src ./venv/bin/uvicorn main:app --reload


pytest:
	PYTHONPATH=${PWD}/backend/src ./venv/bin/pytest
