# pub_trend
dated: 06/10/2022

published to: https://matesto.site/


## installation
To make local installation as simple as possible use power of make as shown below:

```
make init
```

## run locally
The docker-compose file cannot be used locally as Caddy will not be able to get proper SSL certs.

To check solution locally run (runs backend only):

```
make run_server_locally
```

Backend run locally can be accessed with: 127.0.0.1:8000

## deploy
Currently the app is deployed using docker-compose (anti-pattern and temporary solution), to deploy it use:

```
make up
```


**N.B.** The used url (matesto.site) is hardcoded so code requires adjastments if is to be deployed somewhere else.

## tests
To run unit-tests run:

```
make pytest
```

# misc
Static files of the frontend can be found in `frontend/release`
